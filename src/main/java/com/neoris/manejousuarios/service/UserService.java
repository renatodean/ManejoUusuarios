package com.neoris.manejousuarios.service;
 
import java.util.List;
 
import com.neoris.manejousuarios.model.User;
 
 
public interface UserService {
     
    User findById(Integer id);
    
    User findByUserName(String userName);
     
    void saveUser(User user);
     
    void updateUser(User user);
 
    List<User> findAllUsers();
 
}