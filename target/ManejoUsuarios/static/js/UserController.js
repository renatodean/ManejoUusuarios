//Define an angular module for our app
var myapp = angular.module("App", ['ngRoute']);

myapp.run(templateListado);

templateListado.$inject = ['$templateCache'];

function templateListado($templateCache){
	$templateCache.put('listado.html', '<div class="btn-group" role="group" aria-label="...">' + 
			  '<button type="button" class="btn btn-default">Left</button>' +
			  '<button type="button" class="btn btn-default">Middle</button>' +
			  '<button type="button" class="btn btn-default">Right</button>' +
			  '<div>{{message}}</div>' +
			'</div>');
	$templateCache.put('crear-usuario.html', '<form ng-submit="usuario.submit()"><div><label>Usuario</label><div><input type="text" placeholder="Ingrese Usuario" ng-model="usuario.username"></div></div><div><label>Password</label><div><input type="password" ng-model="usuario.password"></div></div><div><label>Nombre</label><div><input type="text" placeholder="Ingrese Nombre" ng-model="usuario.name"></div></div><div><label>Apellido</label><div><input type="text" placeholder="Ingrese Apellido" ng-model="usuario.lastname"></div></div><div><label>Email</label><div><input type="email" placeholder="example@example.com" ng-model="usuario.email"></div></div><div><label>Edad</label><div><input type="number" placeholder="25" ng-model="usuario.edad"></div></div><div><input type="submit" value="Submit"></div></form>');
};

myapp.config(configuracion);

configuracion.$inject = ['$routeProvider'];

function configuracion($routeProvider){
	$routeProvider.
    when('/listado', {
  	  templateUrl: 'listado.html',
  	  controller: 'AddOrderController'
	}).when('/agregar', {
		templateUrl: 'crear-usuario.html',
	  	controller: 'MiController'
	}).
    otherwise({
		redirectTo: '/'
    });
};

myapp.controller('AddOrderController', AddOrderController);

AddOrderController.$inject = ['$scope'];

function AddOrderController($scope){
	$scope.message = 'aca habia una variable que meti en cache';
};

myapp.controller('MiController',MiController);
	
MiController.$inject = ['$scope', '$http'];

function MiController($scope, $http){
	$scope.usuario.submit = function(){
		$http.post('localhost:8080/ManejoUsuarios/usuario/', usuario);
	}
	};