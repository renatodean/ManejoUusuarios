<html lang="en">
  <head>
  	<title>AngularJS Routing example</title>
  	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
  </head>

<body ng-app="App" ng-controller="MiController">                                
	<div>
		<jsp:include page="cabecera.jsp" />
	</div>
	<div ng-view></div>
	<div>
		<!-- <jsp:include page="listado.jsp" /> -->
	</div>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.7/angular.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.7/angular-route.js"></script>
    <script src="./static/js/UserController.js"></script>
</body>

</html>