//Define an angular module for our app
var myapp = angular.module("App", ['ngRoute']);

myapp.config(configuracion);

configuracion.$inject = ['$routeProvider'];

function configuracion($routeProvider){
	$routeProvider.
    when('/listado', {
  	  templateUrl: './static/staticViews/listado.html',
  	  controller: 'UserController'
	}).when('/agregar', {
		templateUrl: './static/staticViews/crear-usuario.html',
	  	controller: 'MainController'
	}).when('/login', {
		templateUrl: './static/staticViews/login.html',
	  	controller: 'LoginController'
	}).when('/inicio', {
		templateUrl: './static/staticViews/inicio.html',
	  	controller: 'IndexController'
	}).when('/success', {
		templateUrl: './static/staticViews/success.html',
	  	controller: 'IndexController'
	}).when('/erroruser', {
		templateUrl: './static/staticViews/usuario-existente.html',
	  	controller: 'IndexController'
	}).when('/nologged', {
		templateUrl: './static/staticViews/usuario-no-autenticado.html',
	  	controller: 'IndexController'
	}).
    otherwise({
		redirectTo: '/'
    });
};

myapp.controller('UserController',['$scope', '$http', function ($scope, $http){
	var myUrl = 'http://localhost:8080/ManejoUsuarios/listar/';
	$scope.usuarios = [{}];
	$http.get(myUrl).
	then(function(response) {
		$scope.usuarios = response.data;
	});
}]);

myapp.controller('IndexController',function (){
	
});

myapp.controller('LoginController',['$location', '$route', '$rootScope', '$http', function ($location, $route, $rootScope,$http){
	var vm = this;
	$rootScope.usuarioConectado = false;
	vm.ingresar = ingresar;
	var isAuthenticated = false;
	
	function ingresar(username,password){
		$http.get('http://localhost:8080/ManejoUsuarios/login/'+username+'/'+password).
		then(function(response){
			isAuthenticated = response.data;
			if(isAuthenticated){
				$location.url('/success');
				$rootScope.notConnected = true;
				$rootScope.usuarioConectado = null;
				$route.reload();
			}
			else{
				$location.url('/nologged');
				$rootScope.notConnected = null;
				$rootScope.usuarioConectado = false;
				$route.reload();
			}
		})
		
	}
}]);

myapp.controller('MainController',MainController);
	
MainController.$inject = ['$scope', '$http', '$route', '$location', '$rootScope'];

function MainController($scope, $http, $route, $location, $rootScope){
	var vm = this;
	var myUrl = 'http://localhost:8080/ManejoUsuarios/usuario/';
	var myUrlUnique = 'http://localhost:8080/ManejoUsuarios/unique/';
	vm.submit = submit;
	vm.reset = reset;
	vm.usuario = {};
	
	function submit(usuario){
		$http.get(myUrlUnique+usuario.userName).
			then(function(response){
				if(response.data == true){
					$http.post(myUrl, usuario, {
				        headers: { 'Content-Type': 'application/json'}
				    });
				}
				else{
					$location.url('/erroruser');
					$route.reload();
				}
			});
	}
	
	function reset(usuario){
		usuario = {};
		$route.reload();
    }
};


  