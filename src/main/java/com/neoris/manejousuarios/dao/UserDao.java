package com.neoris.manejousuarios.dao;
 
import java.util.List;
 
import com.neoris.manejousuarios.model.User;
 
 
public interface UserDao {
 
    User findById(Integer bigInteger);
    
    User findByUserName(String userName);
     
    void save(User user);
     
    List<User> findAllUsers();
 
}