package com.neoris.manejousuarios.dao;

import java.util.List;
 
import javax.persistence.NoResultException;
 
import org.springframework.stereotype.Repository;
 
import com.neoris.manejousuarios.model.User;
 
 
@Repository("userDao")
public class UserDaoImpl extends AbstractDao<Integer, User> implements UserDao {
 
    public User findById(Integer id) {
        System.out.println("Id : "+id);
        try{
            User user = (User) getEntityManager()
                    .createQuery("SELECT u FROM User u WHERE u.id LIKE :id")
                    .setParameter("id", id)
                    .getSingleResult();
             
            return user; 
        }catch(NoResultException ex){
            return null;
        }
    }
    
    public User findByUserName(String userName){
    	try{
            User user = (User) getEntityManager()
                    .createQuery("SELECT u FROM User u WHERE u.userName LIKE :userName")
                    .setParameter("userName", userName)
                    .getSingleResult();
             
            return user; 
        }catch(NoResultException ex){
            return new User();
        }
    }
     
    @SuppressWarnings("unchecked")
    public List<User> findAllUsers() {
        List<User> users = getEntityManager()
                .createQuery("SELECT u FROM User u ORDER BY u.firstName DESC")
                .getResultList();
        return users;
    }
 
    public void save(User user) {
        persist(user);
    }
 
}