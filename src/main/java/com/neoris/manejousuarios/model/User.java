package com.neoris.manejousuarios.model;
 
import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

 
@SuppressWarnings("serial")
@Entity
@Table(name="USUARIO")
public class User implements Serializable{
   
    @NotNull
    @Column(name="USERNAME")
    private String userName;
     
    @NotNull
    @Column(name="PASSWORD")
    private String password;
         
    @NotNull
    @Column(name="FIRST_NAME")
    private String firstName;
 
    @NotNull
    @Column(name="LAST_NAME")
    private String lastName;
 
    @NotNull
    @Column(name="EMAIL")
    private String email;
    
    @NotNull
    @Column(name="EDAD")
    private Integer edad;
    
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    
    public User(){
    	this.id = null;
    }
    
    public String getUserName() {
        return userName;
    }
 
    public void setUserName(String userName) {
        this.userName = userName;
    }
 
    public String getPassword() {
        return password;
    }
 
    public void setPassword(String password) {
        this.password = password;
    }
 
    public String getFirstName() {
        return firstName;
    }
 
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
 
    public String getLastName() {
        return lastName;
    }
 
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
 
    public String getEmail() {
        return email;
    }
 
    public void setEmail(String email) {
        this.email = email;
    }
    
    public Integer getEdad() {
        return edad;
    }
 
    public void setEdad(Integer edad) {
        this.edad = edad;
    }
    
    public Integer getId() {
        return id;
    }
 
    public void setId(Integer id) {
        this.id = id;
    }
 
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }
 
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (!(obj instanceof User))
            return false;
        User other = (User) obj;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        return true;
    }
 
    @Override
    public String toString() {
        return "User [id=" + id + ", username=" + userName + ", password=" + password
                + ", firstname=" + firstName + ", lastname=" + lastName
                + ", email=" + email + ", edad=" + edad + "]";
    }
 
 
     
}