package com.neoris.manejousuarios.controller;
 
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
 
 
 
@Controller
public class AppController {

    /**
     * This method will list all existing users.
     */
    @RequestMapping(value = "/")
    public String ShowMain(ModelMap model) {
    	model.addAttribute("title","ManejoUsuarios");
        return "manejousuarios";
    }
 
}