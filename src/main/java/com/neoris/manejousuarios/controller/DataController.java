package com.neoris.manejousuarios.controller;
 
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;


import com.neoris.manejousuarios.model.User;
import com.neoris.manejousuarios.service.UserService;
 
@RestController
@RequestMapping("/")
public class DataController {
 
    @Autowired
    UserService userService; //Service which will do all data retrieval/manipulation work
    
    Logger log = Logger.getLogger(DataController.class.getName());
    // -------------------Create a User-------------------------------------------
 
    @RequestMapping(value = "/usuario", method = RequestMethod.POST)
    @Transactional(readOnly = false)
    public ResponseEntity<User> createUser(@RequestBody User user) {
    	System.out.println(user);
    	userService.saveUser(user);
    	return new ResponseEntity<User>(user,HttpStatus.OK);
    }
    
    @RequestMapping(value = "/listar", method = RequestMethod.GET)
    public @ResponseBody List<User> getUsers() {
    	return userService.findAllUsers();
    }
    
    @RequestMapping(value = "/unique/{userName}", method = RequestMethod.GET)
    public @ResponseBody boolean uniqueUser(@PathVariable("userName") String userName) {
    	if(userService.findByUserName(userName) == null){
    		return true;
    	}
    	else{
    		return false;
    	}
    }
    
    @RequestMapping(value = "/login/{userName}/{password}", method = RequestMethod.GET)
    public @ResponseBody boolean loginUser(@PathVariable("userName") String userName, @PathVariable("password") String password) {
    	User user = userService.findByUserName(userName);
    	if(user.getId() == null){
    		return false;
    	}
    	else{
    		String userPassword = user.getPassword();
    		if(userPassword.equals(password)){
    			return true;
    		}
    		else{
    			return false;
    		}
    		
    	}
    }
    
    @RequestMapping(value = "/usuario/{userName}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<User> getUser(@PathVariable("userName") String userName) {
        User user = userService.findByUserName(userName);
        if (user == null) {
            System.out.println("User with userName " + userName + " not found");
            return new ResponseEntity<User>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<User>(user, HttpStatus.OK);
    }
 
}